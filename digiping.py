#!/usr/bin/env python
import os
#import sys
#import re
#import subprocess
#import time
import requests
import datetime

import pygtk
import gtk
import gobject
from lxml import html
pygtk.require('2.0')

class DigiPing(object):
    def __init__(self):
        self.UNKNOWN = 'disconnected'
        self.DISCONNECTED = 'disconnected'
        self.CONNECTED = 'connected'

        self.icon_filename = {
            self.DISCONNECTED : 'disconnected.png',
            self.CONNECTED : 'connected.png',
        }
        self.DIGI_ID = '__vardas.pavarde__'
        self.location = os.path.dirname(os.path.realpath(__file__))
        self.state = self.UNKNOWN
        
        self.menu = None
        self.CONST_TIMEOUT_MSEC = 30000
        self.connections = 0
        self.update_icon()
        self.icon.set_visible(True)
    
    #def quit action
    def quit_action(self, data, event_button, event_time):
        gtk.main_quit()

    #load images from folder
    def update_icon(self): 
        fn = self.icon_filename[self.state]
        path = os.path.join(self.location, fn)
        assert os.path.exists(path), 'File not found: %s' % path
        if hasattr(self, 'icon'):
            self.icon.set_from_file(path)
        else:
            self.icon = gtk.status_icon_new_from_file(path)
        if self.connections == 0:
            self.icon.connect("popup-menu", self.quit_action)
            self.icon.connect("activate", self.check_site)
            self.connections = 1
        

    def set_active (self):
        print datetime.datetime.now().isoformat() + ' : set_active'
        self.state = self.CONNECTED
        self.update_icon()

    def set_not_active (self):
        print datetime.datetime.now().isoformat() + ' : set_not_active'
        self.state = self.DISCONNECTED
        self.update_icon()

    def check_site(self, widget = 0):
        try:
             page = requests.get('http://time.dgn.lt/?username='+self.DIGI_ID)
        except requests.exceptions.RequestException as e:
              self.set_not_active()
              return True

        if page.status_code != 200:
              self.set_not_active()
              return True

        tree = html.fromstring(page.content)
        week_day = (datetime.datetime.today().weekday())
        digihtml = tree.xpath('//*/div[@class="day"]')[week_day]

        try:      
              day_time_left = digihtml.xpath('//*/div[@class="time_left"]/time')[0].text.strip()
              week_time_left = digihtml.xpath('//*/div[@class="time_left"]/span')[0].text.strip()
        except:
              self.set_not_active()
              return True

        digihtml = digihtml.xpath('div[@class="intervals"]/div/a')
        if len(digihtml) < 1:
              self.set_not_active()
              return True

        digihtml = digihtml[len(digihtml) - 1].text
        digihtml = digihtml.strip()    
        self.icon.set_tooltip('Status: [today: '+day_time_left+' & week: '+week_time_left+']')
	self.icon.set_tooltip_text('Status: [today: '+day_time_left+' & week: '+week_time_left+']')	

        if len(digihtml) != 5:
            self.set_active()
        else:      
            self.set_not_active()
        return True 

    def main(self):
        self.set_not_active()
        self.check_site()
        gobject.timeout_add(self.CONST_TIMEOUT_MSEC, self.check_site) 
        gtk.main()


if __name__ == "__main__":
    app = DigiPing()
    app.main()
